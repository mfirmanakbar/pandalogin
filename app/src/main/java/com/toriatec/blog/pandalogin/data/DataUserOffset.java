package com.toriatec.blog.pandalogin.data;

/**
 * Created by firmanmac on 2/8/17.
 */

public class DataUserOffset {
    String ids, emails, passwords, token_apis, session_ids, create_dates, update_token_sessions;

    public DataUserOffset(){

    }

    public DataUserOffset(String ids, String emails, String passwords, String token_apis, String session_ids, String create_dates, String update_token_sessions) {
        this.ids = ids;
        this.emails = emails;
        this.passwords = passwords;
        this.token_apis = token_apis;
        this.session_ids = session_ids;
        this.create_dates = create_dates;
        this.update_token_sessions = update_token_sessions;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public String getPasswords() {
        return passwords;
    }

    public void setPasswords(String passwords) {
        this.passwords = passwords;
    }

    public String getToken_apis() {
        return token_apis;
    }

    public void setToken_apis(String token_apis) {
        this.token_apis = token_apis;
    }

    public String getSession_ids() {
        return session_ids;
    }

    public void setSession_ids(String session_ids) {
        this.session_ids = session_ids;
    }

    public String getCreate_dates() {
        return create_dates;
    }

    public void setCreate_dates(String create_dates) {
        this.create_dates = create_dates;
    }

    public String getUpdate_token_sessions() {
        return update_token_sessions;
    }

    public void setUpdate_token_sessions(String update_token_sessions) {
        this.update_token_sessions = update_token_sessions;
    }
}
