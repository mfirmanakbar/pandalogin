package com.toriatec.blog.pandalogin.data;

/**
 * Created by firmanmac on 2/7/17.
 */

public class DataUser {
    String id, email, password, token_api, session_id, create_date, update_token_session;

    public DataUser(){

    }

    public DataUser(String id, String email, String password, String token_api, String session_id, String create_date, String update_token_session) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.token_api = token_api;
        this.session_id = session_id;
        this.create_date = create_date;
        this.update_token_session = update_token_session;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken_api() {
        return token_api;
    }

    public void setToken_api(String token_api) {
        this.token_api = token_api;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getUpdate_token_session() {
        return update_token_session;
    }

    public void setUpdate_token_session(String update_token_session) {
        this.update_token_session = update_token_session;
    }
}
