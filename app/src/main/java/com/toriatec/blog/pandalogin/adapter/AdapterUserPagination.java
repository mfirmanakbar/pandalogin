package com.toriatec.blog.pandalogin.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.toriatec.blog.pandalogin.R;
import com.toriatec.blog.pandalogin.data.DataUserPagination;
import com.toriatec.blog.pandalogin.util.OnLoadMoreListener;

import java.util.List;

/**
 * Created by firmanmac on 2/7/17.
 */

public class AdapterUserPagination extends RecyclerView.Adapter {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private List<DataUserPagination> dataUserPaginations;

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public AdapterUserPagination(List<DataUserPagination> UserPaginations, RecyclerView recyclerView){
        dataUserPaginations = UserPaginations;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading
                            && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return dataUserPaginations.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
            vh = new UsersViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UsersViewHolder) {
            DataUserPagination datanyaUser = (DataUserPagination) dataUserPaginations.get(position);
            ((UsersViewHolder) holder).tvIdpg.setText(datanyaUser.getIdpg());
            ((UsersViewHolder) holder).tvEmailPg.setText(datanyaUser.getEmailpg());
            ((UsersViewHolder) holder).tvPasswordPg.setText(datanyaUser.getPasswordpg());
            ((UsersViewHolder) holder).dataUser = datanyaUser;
        }else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public int getItemCount() {
        return dataUserPaginations.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public static class UsersViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIdpg;
        public TextView tvEmailPg;
        public TextView tvPasswordPg;
        public DataUserPagination dataUser;
        public UsersViewHolder(View v) {
            super(v);
            tvIdpg = (TextView) v.findViewById(R.id.txtItemId);
            tvEmailPg = (TextView) v.findViewById(R.id.txtItemEmail);
            tvPasswordPg = (TextView) v.findViewById(R.id.txtItemPassword);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(),"On Click Bro:" + dataUser.getIdpg() + " \n "+dataUser.getEmailpg(),Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }
}
