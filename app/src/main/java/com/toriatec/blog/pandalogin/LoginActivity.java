package com.toriatec.blog.pandalogin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.toriatec.blog.pandalogin.util.ConfigUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText txtEmailLogins, txtPasswordLogins;
    Button btnSubmitLogins;

    private JSONObject jsonObject;
    private ProgressDialog dialog = null;
    private String TAG = "LoginActivity";

    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtEmailLogins = (EditText) findViewById(R.id.txtEmailLogin);
        txtPasswordLogins = (EditText) findViewById(R.id.txtPasswordLogin);
        btnSubmitLogins = (Button) findViewById(R.id.btnSubmitLogin);

        jsonObject = new JSONObject();

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading ...");
        dialog.setCancelable(false);

        sp = getSharedPreferences("sesiLogin", Context.MODE_PRIVATE);
        String sesiLogin_email = sp.getString("sesiLogin_email", null);
        //Toast.makeText(LoginActivity.this,"Sesi email: "+sesiLogin_email,Toast.LENGTH_LONG).show();
        if(sesiLogin_email!=null){
            Intent a = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(a);
            finish();
        }
    }

    public void onBtnToRegister(View view){
        Intent a = new Intent(LoginActivity.this,RegisterActivity.class);
        startActivity(a);
    }

    public void onBtnLoginClicked(View view) {
        LoginUser(txtEmailLogins.getText().toString(),txtPasswordLogins.getText().toString());
    }

    private void LoginUser(final String email, final String password) {
        try {
            jsonObject.put("email",email);
            jsonObject.put("password",password);
        }catch (JSONException e){
            Log.e(TAG, e.toString());
        }
        dialog.show();
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConfigUrl.link_login, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            Boolean success = response.getBoolean("success");
                            String message = response.getString("message");
                            String email = response.getString("email");

                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString("sesiLogin_email", email);
                            editor.commit();

                            if (success==true){
                                //Toast.makeText(LoginActivity.this,email+", "+message,Toast.LENGTH_LONG).show();
                                Intent a = new Intent(LoginActivity.this,MainActivity.class);
                                startActivity(a);
                                finish();
                            }else{
                                Toast.makeText(LoginActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e(TAG,error.toString());
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);

        final JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.POST, ConfigUrl.link_login, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                    }
                })
            {
            @Override
            public Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                dialog.dismiss();
                Map<String, String> responseHeaders = response.headers;
                Log.d(TAG,"token-api: " + responseHeaders.get("token-api"));
                Log.d(TAG,"session-id: " + responseHeaders.get("session-id"));

                SharedPreferences.Editor editor = sp.edit();
                editor.putString("sesiLogin_token", responseHeaders.get("token-api"));
                editor.putString("sesiLogin_session", responseHeaders.get("session-id"));
                editor.commit();

                return null;
            }
        };
        jsonObjectRequest2.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest2);

    }


}
