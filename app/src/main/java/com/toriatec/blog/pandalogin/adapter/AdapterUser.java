package com.toriatec.blog.pandalogin.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.toriatec.blog.pandalogin.R;
import com.toriatec.blog.pandalogin.data.DataUser;
import com.toriatec.blog.pandalogin.util.OnLoadMoreListener;

import java.util.ArrayList;

/**
 * Created by firmanmac on 2/7/17.
 */

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.MyViewHolder> {

    private ArrayList<DataUser> dataSet;
    Context contexts;
    private OnLoadMoreListener onLoadMoreListener;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtId, txtEmail, txtPassword;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.txtId = (TextView) itemView.findViewById(R.id.txtItemId);
            this.txtEmail = (TextView) itemView.findViewById(R.id.txtItemEmail);
            this.txtPassword = (TextView) itemView.findViewById(R.id.txtItemPassword);
        }
    }

    public AdapterUser(ArrayList<DataUser> data) {
        this.dataSet = data;
    }

    @Override
    public AdapterUser.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        contexts = parent.getContext();
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterUser.MyViewHolder holder, final int position) {
        TextView idtxt = holder.txtId;
        TextView emailtxt = holder.txtEmail;
        TextView passwordtxt = holder.txtPassword;

        idtxt.setText(dataSet.get(position).getId());
        emailtxt.setText(dataSet.get(position).getEmail());
        passwordtxt.setText(dataSet.get(position).getPassword());

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(contexts,"OnClick dari Adapter! " + dataSet.get(position).getEmail(), Toast.LENGTH_LONG).show();
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}
