package com.toriatec.blog.pandalogin.util;

/**
 * Created by firmanmac on 2/6/17.
 */

public class ConfigUrl {

    public static final String iphost = "http://192.168.1.101/";
    public static final String link_login = iphost + "tem_cib/api/login";
    public static final String link_register = iphost + "tem_cib/api/index";
    public static final String link_my_profile = iphost + "tem_cib/api/show_one_user_header";
    public static final String link_pagination_users = iphost + "tem_cib/api/pagination_users";
    public static final String link_show_all_users = iphost + "tem_cib/api/show_all_users";


}

