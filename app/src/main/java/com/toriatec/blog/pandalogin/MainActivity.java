package com.toriatec.blog.pandalogin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.toriatec.blog.pandalogin.adapter.AdapterUser;
import com.toriatec.blog.pandalogin.data.DataUser;
import com.toriatec.blog.pandalogin.util.ConfigUrl;
import com.toriatec.blog.pandalogin.util.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private JSONObject jsonObject;
    private String TAG = "MainActivity";

    SharedPreferences sp;

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    //private LinearLayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataUser> datas = new ArrayList<DataUser>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        jsonObject = new JSONObject();

        sp = getSharedPreferences("sesiLogin", Context.MODE_PRIVATE);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_mains);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter = new AdapterUser(datas);
        recyclerView.setAdapter(adapter);

        final LinearLayout btnBawahMain = (LinearLayout) findViewById(R.id.buttonBawahMain);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy){
                if (dy > 7 ){
                    btnBawahMain.setVisibility(View.GONE);
                }else if (dy < -7 ) {
                    btnBawahMain.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE){
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        final String idx = datas.get(position).getId();
                        Toast.makeText(getApplicationContext(),"ID: "+idx,Toast.LENGTH_SHORT).show();
                    }
                })
        );

        loadData();

    }


    private void loadData() {
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConfigUrl.link_show_all_users, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            Boolean success = response.getBoolean("success");
                            String message = response.getString("message");
                            if (success==true){
                                JSONArray jsonArrayContent = response.getJSONArray("content");
                                for(int i=0; i<jsonArrayContent.length();i++){
                                    JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(i);
                                    String id = jsonObjectContent.getString("id");
                                    String email = jsonObjectContent.getString("email");
                                    String password = jsonObjectContent.getString("password");
                                    String token_api = jsonObjectContent.getString("token_api");
                                    String session_id = jsonObjectContent.getString("session_id");
                                    String create_date = jsonObjectContent.getString("create_date");
                                    String update_token_session = jsonObjectContent.getString("update_token_session");

                                    DataUser item = new DataUser();
                                    item.setId(id);
                                    item.setEmail(email);
                                    item.setPassword(password);
                                    item.setToken_api(token_api);
                                    item.setSession_id(session_id);
                                    item.setCreate_date(create_date);
                                    item.setUpdate_token_session(update_token_session);
                                    datas.add(item);
                                }
                            }else{
                                Toast.makeText(MainActivity.this,message,Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,error.toString());
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    public void onBtnProfileMain(View view){
        Intent a = new Intent(MainActivity.this,MyProfileActivity.class);
        startActivity(a);
    }

    public  void onBtnLogoutMain(View view){
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.commit();
        Intent a = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(a);
        finish();
    }

    public void onBtnPaginationMain(View view){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setMessage("Please, press type of view !");
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "ListView",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent b = new Intent(MainActivity.this,OffsetActivity.class);
                        startActivity(b);
                    }
                });
        builder1.setNegativeButton(
                "RecyclerView",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

}
