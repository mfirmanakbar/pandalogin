package com.toriatec.blog.pandalogin;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.toriatec.blog.pandalogin.util.ConfigUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyProfileActivity extends AppCompatActivity {

    private JSONObject jsonObject;
    private ProgressDialog dialog = null;
    private String TAG = "MyProfileActivity";

    SharedPreferences sp;

    String sesiLogin_token, sesiLogin_session;

    TextView txtHasilMyProfiles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        txtHasilMyProfiles = (TextView) findViewById(R.id.txtHasilMyProfile);

        jsonObject = new JSONObject();

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading ...");
        dialog.setCancelable(false);

        sp = getSharedPreferences("sesiLogin", Context.MODE_PRIVATE);
        String sesiLogin_email = sp.getString("sesiLogin_email", null);
        if (!sesiLogin_email.equals("")){
            loadMyProifile(sesiLogin_email);
        }else {
            Toast.makeText(MyProfileActivity.this,"Sesi email null!",Toast.LENGTH_LONG).show();
        }
        sesiLogin_token = sp.getString("sesiLogin_token", null);
        sesiLogin_session = sp.getString("sesiLogin_session", null);

    }

    private void loadMyProifile(final String sesiLogin_email) {
        try {
            jsonObject.put("email",sesiLogin_email);
        }catch (JSONException e){
            Log.e(TAG, e.toString());
        }
        dialog.show();
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConfigUrl.link_my_profile, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            Boolean success = response.getBoolean("success");
                            String message = response.getString("message");
                            Toast.makeText(MyProfileActivity.this,message,Toast.LENGTH_LONG).show();
                            if (success==true){
                                JSONArray jsonArrayContent = response.getJSONArray("content");
                                JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(0);
                                String id = jsonObjectContent.getString("id");
                                String email = jsonObjectContent.getString("email");
                                String password = jsonObjectContent.getString("password");
                                String token_api = jsonObjectContent.getString("token_api");
                                String session_id = jsonObjectContent.getString("session_id");
                                String create_date = jsonObjectContent.getString("create_date");
                                String update_token_session = jsonObjectContent.getString("update_token_session");
                                String res = "ID: "+id+"\nEmail: "+email+"\nPassword: "+password+
                                        "\nToken API: "+token_api+"\nSession ID: "+session_id+
                                        "\nCreate Date: "+create_date+"\nUpdate Token & Session: "+update_token_session;
                                Log.d(TAG,res);
                                txtHasilMyProfiles.setText(res);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e(TAG,error.toString());
            }
        }){
            @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("session_id", sesiLogin_session);
                    params.put("token_api", sesiLogin_token);
                    return params;
                }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }
}
