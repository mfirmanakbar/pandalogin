package com.toriatec.blog.pandalogin;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.toriatec.blog.pandalogin.adapter.AdapterUserOffset;
import com.toriatec.blog.pandalogin.data.DataUserOffset;
import com.toriatec.blog.pandalogin.util.ConfigUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OffsetActivity extends AppCompatActivity implements  SwipeRefreshLayout.OnRefreshListener{

    ListView list;
    SwipeRefreshLayout swipe;
    List<DataUserOffset> usersList = new ArrayList<DataUserOffset>();

    private static final String TAG = "OffsetActivity";

    private int offSet = 1;

    int no;

    AdapterUserOffset adapter;

    Handler handler;
    Runnable runnable;

    private JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offset);

        jsonObject = new JSONObject();

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_offset);
        list = (ListView) findViewById(R.id.list_offset);
        usersList.clear();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                Toast.makeText(OffsetActivity.this, usersList.get(position).getIds(), Toast.LENGTH_LONG).show();
            }
        });

        adapter = new AdapterUserOffset(OffsetActivity.this, usersList);
        list.setAdapter(adapter);

        swipe.setOnRefreshListener(this);

        swipe.post(new Runnable() {
                       @Override
                       public void run() {
                           swipe.setRefreshing(true);
                           usersList.clear();
                           adapter.notifyDataSetChanged();
                           callData(1);
                       }
                   }
        );

        list.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int currentVisibleItemCount;
            private int currentScrollState;
            private int currentFirstVisibleItem;
            private int totalItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                this.currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                this.currentFirstVisibleItem = firstVisibleItem;
                this.currentVisibleItemCount = visibleItemCount;
                this.totalItem = totalItemCount;
            }

            private void isScrollCompleted() {
                if (totalItem - currentFirstVisibleItem == currentVisibleItemCount
                        && this.currentScrollState == SCROLL_STATE_IDLE) {

                    swipe.setRefreshing(true);
                    handler = new Handler();

                    runnable = new Runnable() {
                        public void run() {
                            callData(offSet);
                        }
                    };

                    handler.postDelayed(runnable, 3000);
                }
            }

        });

    }

    @Override
    public void onRefresh() {
        usersList.clear();
        adapter.notifyDataSetChanged();
        callData(1);
    }

    private void callData(final int index) {
        //offSet=offSet+1;
        swipe.setRefreshing(true);
        try {
            jsonObject.put("page", index);
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConfigUrl.link_pagination_users, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            Boolean success = response.getBoolean("success");
                            String message = response.getString("message");
                            if (success == true) {
                                offSet=offSet+1;
                                JSONArray jsonArrayContent = response.getJSONArray("content");
                                for (int i = 0; i < jsonArrayContent.length(); i++) {
                                    JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(i);
                                    String id = jsonObjectContent.getString("id");
                                    String email = jsonObjectContent.getString("email");
                                    String password = jsonObjectContent.getString("password");
                                    String token_api = jsonObjectContent.getString("token_api");
                                    String session_id = jsonObjectContent.getString("session_id");
                                    String create_date = jsonObjectContent.getString("create_date");
                                    String update_token_session = jsonObjectContent.getString("update_token_session");

                                    Log.d(TAG, id+". "+create_date);

                                    DataUserOffset item = new DataUserOffset();
                                    item.setIds(id);
                                    item.setEmails(email);
                                    item.setPasswords(password);
                                    item.setToken_apis(token_api);
                                    item.setSession_ids(session_id);
                                    item.setCreate_dates(create_date);
                                    item.setUpdate_token_sessions(update_token_session);
                                    usersList.add(item);

                                }
                            } else {
                                //Toast.makeText(OffsetActivity.this, message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapter.notifyDataSetChanged();
                        swipe.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
                swipe.setRefreshing(false);
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }


}
