package com.toriatec.blog.pandalogin.data;

import java.io.Serializable;

/**
 * Created by firmanmac on 2/7/17.
 */

public class DataUserPagination implements Serializable {
    String idpg, emailpg, passwordpg, token_apipg, session_idpg, create_datepg, update_token_sessionpg;

    public DataUserPagination() {
    }

    public DataUserPagination(String idpg, String emailpg, String passwordpg, String token_apipg, String session_idpg, String create_datepg, String update_token_sessionpg) {
        this.idpg = idpg;
        this.emailpg = emailpg;
        this.passwordpg = passwordpg;
        this.token_apipg = token_apipg;
        this.session_idpg = session_idpg;
        this.create_datepg = create_datepg;
        this.update_token_sessionpg = update_token_sessionpg;
    }

    public String getIdpg() {
        return idpg;
    }

    public void setIdpg(String idpg) {
        this.idpg = idpg;
    }

    public String getEmailpg() {
        return emailpg;
    }

    public void setEmailpg(String emailpg) {
        this.emailpg = emailpg;
    }

    public String getPasswordpg() {
        return passwordpg;
    }

    public void setPasswordpg(String passwordpg) {
        this.passwordpg = passwordpg;
    }

    public String getToken_apipg() {
        return token_apipg;
    }

    public void setToken_apipg(String token_apipg) {
        this.token_apipg = token_apipg;
    }

    public String getSession_idpg() {
        return session_idpg;
    }

    public void setSession_idpg(String session_idpg) {
        this.session_idpg = session_idpg;
    }

    public String getCreate_datepg() {
        return create_datepg;
    }

    public void setCreate_datepg(String create_datepg) {
        this.create_datepg = create_datepg;
    }

    public String getUpdate_token_sessionpg() {
        return update_token_sessionpg;
    }

    public void setUpdate_token_sessionpg(String update_token_sessionpg) {
        this.update_token_sessionpg = update_token_sessionpg;
    }
}
