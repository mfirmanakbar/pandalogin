package com.toriatec.blog.pandalogin.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.toriatec.blog.pandalogin.R;
import com.toriatec.blog.pandalogin.data.DataUserOffset;

import java.util.List;

/**
 * Created by firmanmac on 2/8/17.
 */

public class AdapterUserOffset extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<DataUserOffset> offsetsItems;

    public AdapterUserOffset(Activity activity, List<DataUserOffset> offsets) {
        this.activity = activity;
        this.offsetsItems = offsets;
    }

    @Override
    public int getCount() {
        return offsetsItems.size();
    }

    @Override
    public Object getItem(int position) {
        return offsetsItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null)
            convertView = inflater.inflate(R.layout.item_main, null);

        TextView id = (TextView) convertView.findViewById(R.id.txtItemId);
        TextView email = (TextView) convertView.findViewById(R.id.txtItemEmail);
        TextView password = (TextView) convertView.findViewById(R.id.txtItemPassword);

        DataUserOffset dt = offsetsItems.get(position);
        id.setText(dt.getIds());
        email.setText(dt.getEmails());
        password.setText(dt.getPasswords());

        return convertView;

    }
}
