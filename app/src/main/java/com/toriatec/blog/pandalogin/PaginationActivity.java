package com.toriatec.blog.pandalogin;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.toriatec.blog.pandalogin.adapter.AdapterUserPagination;
import com.toriatec.blog.pandalogin.data.DataUserPagination;
import com.toriatec.blog.pandalogin.util.ConfigUrl;
import com.toriatec.blog.pandalogin.util.OnLoadMoreListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class PaginationActivity extends AppCompatActivity {


    String TAG = "PaginationActivity";
    private JSONObject jsonObject;

    private TextView tvEmptyView;
    private RecyclerView mRecyclerView;
    private AdapterUserPagination mAdapter;
    private LinearLayoutManager mLayoutManager;

    private List<DataUserPagination> usersList;

    protected Handler handler;

    int indexPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagination);

        jsonObject = new JSONObject();

        tvEmptyView = (TextView) findViewById(R.id.empty_view);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        usersList = new ArrayList<DataUserPagination>();
        handler = new Handler();

        loadData(1);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new AdapterUserPagination(usersList, mRecyclerView);
        mRecyclerView.setAdapter(mAdapter);

        if (usersList.isEmpty()) {
            mRecyclerView.setVisibility(View.GONE);
            tvEmptyView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            tvEmptyView.setVisibility(View.GONE);
        }

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                //add null , so the adapter will check view_type and show progress bar at bottom
//                usersList.add(null);
//                mAdapter.notifyItemInserted(usersList.size() - 1);
//                //   remove progress item
//                usersList.remove(usersList.size() - 1);
//                mAdapter.notifyItemRemoved(usersList.size());
                //loadData(indexPage);
            }
        });

    }

    private void loadData(final int index) {
        try {
            jsonObject.put("page", "1");
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConfigUrl.link_pagination_users, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            Boolean success = response.getBoolean("success");
                            String message = response.getString("message");
                            Toast.makeText(PaginationActivity.this, response.toString(), Toast.LENGTH_LONG).show();
                            if (success == true) {
                                JSONArray jsonArrayContent = response.getJSONArray("content");
                                for (int i = 0; i < jsonArrayContent.length(); i++) {
                                    JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(i);
                                    String id = jsonObjectContent.getString("id");
                                    String email = jsonObjectContent.getString("email");
                                    String password = jsonObjectContent.getString("password");
                                    String token_api = jsonObjectContent.getString("token_api");
                                    String session_id = jsonObjectContent.getString("session_id");
                                    String create_date = jsonObjectContent.getString("create_date");
                                    String update_token_session = jsonObjectContent.getString("update_token_session");

                                    Log.d(TAG, id+". "+create_date);

                                    DataUserPagination item = new DataUserPagination();
                                    item.setIdpg(id);
                                    item.setEmailpg(email);
                                    item.setPasswordpg(password);
                                    item.setToken_apipg(token_api);
                                    item.setSession_idpg(session_id);
                                    item.setCreate_datepg(create_date);
                                    item.setUpdate_token_sessionpg(update_token_session);
                                    usersList.add(item);
                                    //mAdapter.notifyDataSetChanged();
                                }
                                indexPage+=1;
                            } else {
                                Toast.makeText(PaginationActivity.this, message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }

    /*
    private void load_sample(int index){
        try {
            jsonObject.put("page", index+"");
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, ConfigUrl.link_pagination_users, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        try {
                            Boolean success = response.getBoolean("success");
                            String message = response.getString("message");
                            if (success == true) {
                                JSONArray jsonArrayContent = response.getJSONArray("content");
                                for (int i = 0; i < jsonArrayContent.length(); i++) {
                                    JSONObject jsonObjectContent = jsonArrayContent.getJSONObject(i);
                                    String id = jsonObjectContent.getString("id");
                                    String email = jsonObjectContent.getString("email");
                                    String password = jsonObjectContent.getString("password");
                                    String token_api = jsonObjectContent.getString("token_api");
                                    String session_id = jsonObjectContent.getString("session_id");
                                    String create_date = jsonObjectContent.getString("create_date");
                                    String update_token_session = jsonObjectContent.getString("update_token_session");

                                    DataUserPagination item = new DataUserPagination();
                                    item.setIdpg(id);
                                    item.setEmailpg(email);
                                    item.setPasswordpg(password);
                                    item.setToken_apipg(token_api);
                                    item.setSession_idpg(session_id);
                                    item.setCreate_datepg(create_date);
                                    item.setUpdate_token_sessionpg(update_token_session);
                                    usersList.add(item);
                                    //adapter.notifyDataChanged();
                                }
                            } else {
                                Toast.makeText(PaginationActivity.this, message, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, error.toString());
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(jsonObjectRequest);
    }
    */
}
