package com.toriatec.blog.pandalogin;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.toriatec.blog.pandalogin.util.ConfigUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText txtEmailRegis, txtPasswordRegis;
    Button btnSubmitRegis;

    private JSONObject jsonObject;
    private ProgressDialog dialog = null;
    private String TAG = "RegisterActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        txtEmailRegis = (EditText) findViewById(R.id.txtEmailRegister);
        txtPasswordRegis = (EditText) findViewById(R.id.txtPasswordRegister);
        btnSubmitRegis = (Button) findViewById(R.id.btnSubmitRegister);

        jsonObject = new JSONObject();

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading ...");
        dialog.setCancelable(false);
    }

    public void onBtnToLogin(View view){
        finish();
    }

    public void onBtnRegisterClicked(View view){
        RegisterUserString(txtEmailRegis.getText().toString(),txtPasswordRegis.getText().toString());
    }


    private void RegisterUserString(final String email, final String password) {
        dialog.show();
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, ConfigUrl.link_register, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, response.toString());
                try{
                    JSONObject res = new JSONObject(response);
                    Boolean success = res.getBoolean("success");
                    String message = res.getString("message");
                    if (success==true){
                        Toast.makeText(RegisterActivity.this,message,Toast.LENGTH_LONG).show();
                        finish();
                    }else {
                        Toast.makeText(RegisterActivity.this,message,Toast.LENGTH_LONG).show();
                    }
                }catch (JSONException e){
                    Log.e(TAG,e.getMessage());
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e(TAG,error.toString());
            }
        }) {
                @Override
                protected Map<String,String> getParams(){
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("email",email);
                    params.put("password",password);
                    return params;
                }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Volley.newRequestQueue(this).add(stringRequest);

    }

}
